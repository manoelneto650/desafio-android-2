package com.desafio.dribble;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.desafio.dribble.adapters.ShotsAdapter;
import com.desafio.dribble.model.Popular;
import com.desafio.dribble.rest.IClientAppRest;
import com.desafio.dribble.utils.TarefaShotRest;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.concurrent.ExecutionException;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.corpoDialogLogin)
    protected LinearLayout overlayProgress;

    @ViewById(R.id.progressBar)
    protected ProgressWheel progressBar;

    @ViewById(R.id.rv_list)
    protected RecyclerView listaShotsPopulares;

    @RestService
    protected IClientAppRest serviceRest;

    private boolean primeiraVez = true;
    private ShotsAdapter shotsAdapter;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("primeiravez", primeiraVez);
        outState.putSerializable("shotAdapter", (ShotsAdapter) listaShotsPopulares.getAdapter());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        primeiraVez = outState.getBoolean("primeiravez");
        shotsAdapter = (ShotsAdapter) outState.getSerializable("shotAdapter");
    }

    @AfterViews
    void afterViews() {
        GridLayoutManager llm = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        listaShotsPopulares.setHasFixedSize(true);
        listaShotsPopulares.setLayoutManager(llm);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (primeiraVez || (shotsAdapter == null)) {
            habilitaProgress();

            try {
                getShotsPopulares();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            primeiraVez = false;
        } else {
            listaShotsPopulares.setAdapter(shotsAdapter);
        }

    }

    public void habilitaProgress() {
        progressBar.setVisibility(View.VISIBLE);
        overlayProgress.setVisibility(View.VISIBLE);
        overlayProgress.setBackgroundColor(Color.TRANSPARENT);
        overlayProgress.bringToFront();
    }

    public void desabilitaProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        overlayProgress.setVisibility(View.INVISIBLE);
    }


    private void getShotsPopulares() throws ExecutionException, InterruptedException {

        new TarefaShotRest(serviceRest) {
            @Override
            protected void onPostExecute(final Object object) {
                super.onPostExecute(object);
                ShotsAdapter adapter = new ShotsAdapter(getApplicationContext(), ((Popular) object).getShots());
                listaShotsPopulares.setAdapter(adapter);
                desabilitaProgress();
            }
        }.execute();

    }
}


















