package com.desafio.dribble.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ManoelNeto on 08/09/15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Popular {

    private Shots[] shots;

    public Popular() {

    }

    @JsonProperty("shots")
    public Shots[] getShots() {
        return shots;
    }

    public void setShots(Shots[] shots) {
        this.shots = shots;
    }

}
