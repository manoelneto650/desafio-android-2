package com.desafio.dribble;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.desafio.dribble.model.Shots;
import com.desafio.dribble.rest.IClientAppRest;
import com.desafio.dribble.utils.TarefaShotRest;
import com.desafio.dribble.utils.VolleySingleton;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.concurrent.ExecutionException;

/**
 * Created by ManoelNeto on 10/09/15.
 */

@EActivity(R.layout.activity_detalhes_shot)
public class DetalhesShotActivity extends AppCompatActivity {

    @ViewById(R.id.profile_image)
    protected NetworkImageView profileImage;

    @ViewById(R.id.nome_player)
    protected TextView nomePlayer;

    @ViewById(R.id.title_shot)
    protected TextView titleShot;

    @ViewById(R.id.image_shot_detalhes)
    protected NetworkImageView imageShotDetalhes;

    @Extra("shotSelecionado")
    Shots shotSelecionado;

    @RestService
    protected IClientAppRest serviceRest;

    @AfterViews
    void afterViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        try {
            getShots();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void setaTextos(final Shots shotSelect) {
        nomePlayer.setText(shotSelect.getPlayer().getName());
        titleShot.setText(shotSelect.getTitle());
        profileImage.setImageUrl(shotSelect.getPlayer().getAvatar_url(), VolleySingleton.getInstance().getImageLoader());
        imageShotDetalhes.setImageUrl(shotSelect.getImage_url(), VolleySingleton.getInstance().getImageLoader());
    }

    private void getShots() throws ExecutionException, InterruptedException {
        new TarefaShotRest(serviceRest) {

            @Override
            protected Object doInBackground(String... params) {
                return getRestClient().shotPorId(shotSelecionado.getId());
            }

            @Override
            protected void onPostExecute(Object object) {
                super.onPostExecute(object);
                setaTextos((Shots) object);
            }
        }.execute();
    }


}
