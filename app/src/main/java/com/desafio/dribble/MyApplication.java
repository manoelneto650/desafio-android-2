package com.desafio.dribble;

import android.app.Application;
import android.content.Context;

import java.io.Serializable;

/**
 * Created by ManoelNeto on 15/09/15.
 */
public class MyApplication extends Application implements Serializable {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
