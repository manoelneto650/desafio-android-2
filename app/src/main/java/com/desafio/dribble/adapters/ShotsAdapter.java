package com.desafio.dribble.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.desafio.dribble.DetalhesShotActivity_;
import com.desafio.dribble.R;
import com.desafio.dribble.model.Shots;
import com.desafio.dribble.utils.VolleySingleton;

import java.io.Serializable;


/**
 * Created by Manoel Neto on 09/09/2015.
 */
public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.MyViewHolder> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Context context;
    private Shots[] listaShots;

    public ShotsAdapter(Context c, Shots[] l) {
        context = c;
        listaShots = l;
    }

    //Quando a nescessidade de criar uma nova view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_shot, viewGroup, false);
        return new MyViewHolder(view);
    }

    //Vincula os dados a View
    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {
        Shots shotPosition = listaShots[position];
        viewHolder.views_count.setText(String.valueOf(shotPosition.getViews_count()));
        viewHolder.likes_count.setText(String.valueOf(shotPosition.getLikes_count()));
        viewHolder.image_Shot.setImageUrl(shotPosition.getImage_url(), VolleySingleton.getInstance().getImageLoader());
    }

    @Override
    public int getItemCount() {
        return listaShots.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public NetworkImageView image_Shot;
        public TextView views_count;
        public TextView likes_count;

        public MyViewHolder(View itemView) {
            super(itemView);
            image_Shot = (NetworkImageView) itemView.findViewById(R.id.image_shot);
            views_count = (TextView) itemView.findViewById(R.id.views_count);
            likes_count = (TextView) itemView.findViewById(R.id.likes_count);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DetalhesShotActivity_.intent(context)
                            .flags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .shotSelecionado(listaShots[getPosition()])
                            .start();
                }
            });


        }
    }

}
