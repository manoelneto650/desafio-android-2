package com.desafio.dribble.utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.desafio.dribble.MyApplication;

/**
 * Created by ManoelNeto on 15/09/15.
 */
public class VolleySingleton {

    private static VolleySingleton instancia = null;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private VolleySingleton() {
        requestQueue = Volley.newRequestQueue(MyApplication.getContext());
        imageLoader = new ImageLoader(this.requestQueue, new ImageLoader.ImageCache() {

            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    public static synchronized VolleySingleton getInstance() {
        if (instancia == null) {
            instancia = new VolleySingleton();
        }
        return instancia;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

}
