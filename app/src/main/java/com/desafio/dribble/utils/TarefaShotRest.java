package com.desafio.dribble.utils;

import android.os.AsyncTask;

import com.desafio.dribble.rest.IClientAppRest;

/**
 * Created by Manoel Neto on 11/09/15.
 */
public class TarefaShotRest extends AsyncTask<String, Void, Object> {

    private IClientAppRest restClient;


    public TarefaShotRest(IClientAppRest restClient) {
        this.restClient = restClient;
    }

    @Override
    protected Object doInBackground(String... params) {
        return restClient.shotsPopulares();
    }

    @Override
    protected void onPostExecute(Object object) {
        super.onPostExecute(object);
    }

    public IClientAppRest getRestClient() {
        return restClient;
    }

    public void setRestClient(IClientAppRest restClient) {
        this.restClient = restClient;
    }

}
